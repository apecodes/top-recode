#pragma once

#include <Rpc.h>
#include <string>

class Win32Guid {
public:
	Win32Guid();
	~Win32Guid();

	void Generate();

	std::string AsString() const;

private:
	UUID uuid;
};
